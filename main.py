from jose import JWTError, jwt 
from pydantic import BaseModel
from datetime import datetime, timedelta
from passlib.context import CryptContext
from fastapi import Depends, FastAPI, HTTPException, status
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm

SECRET_KEY = "597affc45331bd6dbc9d18592c81992ef743d0c5264dec4d3d544e9fe4d8445d"
ALGORITHM =  "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 30

db = {
    "tim": {
      "username": "tim",
      "full_name": "Tim Rusica",
      "email": "tim@gmail.com",
      "hashed_password": "$2b$12$PhrcJtDOeKvSxSzHHKdjDeCh7RYQ8Ue9s3PLvBijYpfIGyZH34qgq",
      "disabled": False
    }
}

# username : tim
# password : tim123

class Token(BaseModel):
  access_token: str
  token_type: str

class TokenData(BaseModel):
  username: str | None = None

class User(BaseModel):
  username: str
  email: str | None = None
  full_name: str | None = None
  disabled: bool | None = None

class UserInDB(User):
  hashed_password: str

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")

app = FastAPI()

def verify_password(plain, hashed):
  return pwd_context.verify(plain, hashed)

def get_password_hash(password):
  return pwd_context.hash(password)

def get_user(db, username:str):
  if username in db:
    user_data = db[username]
    return UserInDB(**user_data)

def aunthenticaate_user(db, username: str, password:str):
  user = get_user(db, username)
  if not user:
    return False
  
  if not verify_password(password, user.hashed_password):
    return False
  
  return user

def create_access_token(data: dict, expires_delta: timedelta | None = None):
  to_encode = data.copy()
  if expires_delta:
    expire = datetime.utcnow() + expires_delta
  else:
    expire = datetime.utcnow() + timedelta(minutes=15)

  to_encode.update({"exp": expire})
  encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)

  return encoded_jwt
  
async def get_current_user(token: str = Depends(oauth2_scheme)):
  credential_exception = HTTPException(status_code=status.HTTP_401_UNAUTHORIZED,  detail="Could not validate credentials", headers={"WWW-Authenticate": "Bearer"})

  try:
    payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
    username: str = payload.get("sub")
    if username is None:
      raise credential_exception
    token_data = TokenData(username=username)
  except JWTError:
    raise credential_exception

  user = get_user(db, username=token_data.username)
  if user is None:
    raise credential_exception

  return user

async def get_current_active_user(current_user: UserInDB = Depends(get_current_user)):
  if current_user.disabled:
    raise HTTPException(status_code=400, detail="Inactive user")
  
  return current_user

@app.post("/token", response_model=Token)
async def login_for_access_token(form_data: OAuth2PasswordRequestForm = Depends()):
  user = aunthenticaate_user(db, form_data.username, form_data.password)
  if not user:
    raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Incorrect username or password", headers={"WWW-Authenticate": "Bearer"})
  
  access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
  access_token = create_access_token(data={"sub": user.username}, expires_delta= access_token_expires)
  return {"access_token": access_token, "token_type": "bearer"}

@app.get("/users/me", response_model=User)
async def read_user_me(current_user: User = Depends(get_current_active_user)):
  return current_user

@app.get("/users/me/items")
async def read_own_items(current_user: User = Depends(get_current_active_user)):
  return [{"item_id": 1, "owner": current_user}]